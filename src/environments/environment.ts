// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBgkJB30QcllKxrc6Kxw5O3lFug14qVe2w',
    authDomain: 'code-quest-platform.firebaseapp.com',
    databaseURL: 'https://code-quest-platform.firebaseio.com',
    projectId: 'code-quest-platform',
    storageBucket: 'code-quest-platform.appspot.com',
    messagingSenderId: '783769061034',
    appId: '1:783769061034:web:ad039bc1197e538ecb32d3',
    measurementId: 'G-ECK1XRTZBQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
