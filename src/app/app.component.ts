import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'cq-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'codequest';


}
