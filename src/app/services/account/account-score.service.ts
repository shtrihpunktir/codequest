import { Observable } from 'rxjs';

export abstract class AccountScoreService {
  abstract getScore(): Observable<number>;
}
