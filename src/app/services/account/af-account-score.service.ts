import { AccountScoreService } from './account-score.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { pluck, shareReplay, startWith, switchMap } from 'rxjs/operators';

@Injectable()
export class AfAccountScoreService extends AccountScoreService {
  private score = this.auth.authState.pipe(
    pluck('uid'),
    switchMap(uid => this.firestore
      .doc<{ value: number }>(`account/${uid}`)
      .valueChanges()
      .pipe(
        startWith({value: 0}),
        pluck('value')
      )
    ),
    shareReplay({refCount: false, bufferSize: 1})
  );

  constructor(
    private firestore: AngularFirestore,
    private auth: AngularFireAuth,
  ) {
    super();
  }

  getScore(): Observable<number> {
    return this.score;
  }

}
