import { Observable } from 'rxjs';
import { Product } from '../../models/product';

export abstract class ProductService {
  abstract load(): Observable<Product[]>;

  abstract getById(id: string): Observable<Product>;

  abstract buy(product: Product): Observable<any>;
}
