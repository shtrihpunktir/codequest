import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { ProductService } from './product.service';
import { Product } from '../../models/product';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable()
export class AfProductService extends ProductService {

  private collection = this.firestore.collection<Product>('products');
  private buy$ = this.fns.httpsCallable('buyProduct');

  constructor(
    private firestore: AngularFirestore,
    private fns: AngularFireFunctions
  ) {
    super();
  }

  getById(id: string): Observable<Product> {
    return this.collection.doc<Product>(id).valueChanges().pipe(map(data => ({...data, id})));
  }

  load(): Observable<Product[]> {
    return this.collection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product;
        const id = a.payload.doc.id;
        return {id, ...data};
      }))
    );
  }

  buy(product: Product): Observable<any> {
    return this.buy$({product: product.id});
  }


}
