import { Injectable } from '@angular/core';
import { PromotionService } from './promotion.service';
import { Observable } from 'rxjs';
import { Promotion } from '../../models/promotion';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class AfPromotionService extends PromotionService {

  private collection = this.firestore.collection<Promotion>('promotions');

  constructor(
    private firestore: AngularFirestore
  ) {
    super();
  }

  getById(id: string): Observable<Promotion> {
    return this.collection.doc<Promotion>(id).valueChanges();
  }

  load(): Observable<Promotion[]> {
    return this.collection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Promotion;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

}
