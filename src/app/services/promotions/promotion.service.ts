import { Observable } from 'rxjs';
import { Promotion } from '../../models/promotion';

export abstract class PromotionService {
  abstract load(): Observable<Promotion[]>;

  abstract getById(id: string): Observable<Promotion>;
}
