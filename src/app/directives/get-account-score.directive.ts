import { Directive, EmbeddedViewRef, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountScoreService } from '../services/account/account-score.service';

interface GetAccountScoreContext {
  $implicit: number;
}

@Directive({
  selector: '[cqGetAccountScore]'
})
export class GetAccountScoreDirective implements OnInit, OnDestroy {
  private context: GetAccountScoreContext = {$implicit: 0};
  private viewRef: EmbeddedViewRef<GetAccountScoreContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<GetAccountScoreContext>,
    private score: AccountScoreService
  ) {
  }

  ngOnInit(): void {
    this.subscription = this.score.getScore().subscribe(value => {
      this.context.$implicit = value;
      if (!this.viewRef) {
        this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }
}
