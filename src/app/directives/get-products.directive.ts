import { Directive, EmbeddedViewRef, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Product } from '../models/product';
import { ProductService } from '../services/products/product.service';
import { Subscription } from 'rxjs';

interface GetProductsContext {
  $implicit: Product[];
  buy: (product: Product) => void;
}

@Directive({
  selector: '[cqGetProducts]'
})
export class GetProductsDirective implements OnInit, OnDestroy {
  private context: GetProductsContext = {
    $implicit: null,
    buy: product => this.products.buy(product).subscribe()
  };
  private viewRef: EmbeddedViewRef<GetProductsContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<GetProductsContext>,
    private products: ProductService
  ) {
  }

  ngOnInit(): void {
    this.subscription = this.products.load().subscribe(
      (products) => {
        this.context.$implicit = products;
        if (!this.viewRef) {
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }
}
