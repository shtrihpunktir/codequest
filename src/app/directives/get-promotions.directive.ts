import { Directive, EmbeddedViewRef, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Promotion } from '../models/promotion';
import { PromotionService } from '../services/promotions/promotion.service';
import { Subscription } from 'rxjs';

interface GetPromotionsContext {
  $implicit: Promotion[];
}

@Directive({
  selector: '[cqGetPromotions]'
})
export class GetPromotionsDirective implements OnInit, OnDestroy {
  private context: GetPromotionsContext = {
    $implicit: null
  };
  private viewRef: EmbeddedViewRef<GetPromotionsContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<GetPromotionsContext>,
    private promotions: PromotionService
  ) {
  }

  ngOnInit(): void {
    this.subscription = this.promotions.load().subscribe(
      (promotions) => {
        this.context.$implicit = promotions;
        if (!this.viewRef) {
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }
}
