import { Directive, EmbeddedViewRef, Input, OnChanges, OnDestroy, SimpleChanges, TemplateRef, ViewContainerRef } from '@angular/core';
import { Product } from '../models/product';
import { Subscription } from 'rxjs';
import { ProductService } from '../services/products/product.service';

interface GetProductContext {
  $implicit: Product;
  buy: (product: Product, handler?: () => void) => any;
}

@Directive({
  selector: '[cqGetProduct]'
})
export class GetProductByIdDirective implements OnChanges, OnDestroy {
  @Input() cqGetProductBy: string;

  private context: GetProductContext = {
    $implicit: null,
    buy: (product, handler) => this.products.buy(product).subscribe(handler)
  };
  private viewRef: EmbeddedViewRef<GetProductContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<GetProductContext>,
    private products: ProductService
  ) {
  }


  ngOnChanges(changes: SimpleChanges): void {
    if ('cqGetProductBy' in changes) {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.subscription = this.products.getById(this.cqGetProductBy).subscribe(promotion => {
        this.context.$implicit = promotion;
        if (!this.viewRef) {
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.dispose();
  }

  private dispose() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }
}
