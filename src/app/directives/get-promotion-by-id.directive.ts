import { Directive, EmbeddedViewRef, Input, OnChanges, OnDestroy, SimpleChanges, TemplateRef, ViewContainerRef } from '@angular/core';
import { Promotion } from '../models/promotion';
import { PromotionService } from '../services/promotions/promotion.service';
import { Subscription } from 'rxjs';

interface GetPromotionByIdContext {
  $implicit: Promotion;
}

@Directive({
  selector: '[cqGetPromotion]'
})
export class GetPromotionByIdDirective implements OnChanges, OnDestroy {

  @Input()
  cqGetPromotionBy;
  private context: GetPromotionByIdContext = {
    $implicit: null
  };
  private viewRef: EmbeddedViewRef<GetPromotionByIdContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<GetPromotionByIdContext>,
    private promotions: PromotionService
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('cqGetPromotionBy' in changes) {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.subscription = this.promotions.getById(this.cqGetPromotionBy).subscribe(promotion => {
        this.context.$implicit = promotion;
        if (!this.viewRef) {
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.dispose();
  }


  private dispose() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }
}
