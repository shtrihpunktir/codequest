import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './pages/login/login-page.component';
import { HomePageComponent } from './pages/home/home-page.component';
import { PromotionListPageComponent } from './pages/promotion-list/promotion-list-page.component';
import { PromotionDetailPageComponent } from './pages/promotion-detail/promotion-detail-page.component';
import { CodeRegistrationPageComponent } from './pages/code-registration/code-registration-page.component';
import { ShopPageComponent } from './pages/shop/shop-page.component';
import { ProductDetailsPageComponent } from './pages/product-details/product-details-page.component';
import { AuthenticatedLayoutComponent } from './layout/authenticated-layout/authenticated-layout.component';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/not-auth.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [NotAuthGuard]
  },
  {
    path: '',
    component: AuthenticatedLayoutComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent
      },
      {
        path: 'promotion',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: PromotionListPageComponent
          },
          {
            path: ':promotion',
            component: PromotionDetailPageComponent
          }
        ]
      },
      {
        path: 'register-code',
        component: CodeRegistrationPageComponent
      },
      {
        path: 'shop',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: ShopPageComponent
          },
          {
            path: ':product',
            component: ProductDetailsPageComponent
          }
        ]
      },
      {path: '**', redirectTo: ''}
    ]
  }
];

export const AppRoutingModule = RouterModule.forRoot(routes);
