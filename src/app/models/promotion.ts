export interface Promotion {
  id: string;
  title: string;
  listImage: string;
  listSubtitle: string;
  image: string;
  description: string;

}
