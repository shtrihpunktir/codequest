import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login/login-page.component';
import { HomePageComponent } from './pages/home/home-page.component';
import { PromotionListPageComponent } from './pages/promotion-list/promotion-list-page.component';
import { PromotionDetailPageComponent } from './pages/promotion-detail/promotion-detail-page.component';
import { CodeRegistrationPageComponent } from './pages/code-registration/code-registration-page.component';
import { ShopPageComponent } from './pages/shop/shop-page.component';
import { ProductDetailsPageComponent } from './pages/product-details/product-details-page.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticatedLayoutComponent } from './layout/authenticated-layout/authenticated-layout.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { GetPromotionsDirective } from './directives/get-promotions.directive';
import { GetPromotionByIdDirective } from './directives/get-promotion-by-id.directive';
import { GetProductsDirective } from './directives/get-products.directive';
import { GetProductByIdDirective } from './directives/get-product-by-id.directive';
import { PromotionService } from './services/promotions/promotion.service';
import { AfPromotionService } from './services/promotions/af-promotion.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ProductService } from './services/products/product.service';
import { AfProductService } from './services/products/af-product.service';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { GetAccountScoreDirective } from './directives/get-account-score.directive';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountScoreService } from './services/account/account-score.service';
import { AfAccountScoreService } from './services/account/af-account-score.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    PromotionListPageComponent,
    PromotionDetailPageComponent,
    CodeRegistrationPageComponent,
    ShopPageComponent,
    ProductDetailsPageComponent,
    AuthenticatedLayoutComponent,
    GetPromotionsDirective,
    GetPromotionByIdDirective,
    GetProductsDirective,
    GetProductByIdDirective,
    GetAccountScoreDirective
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireAuthGuardModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireFunctionsModule,
    NgbModalModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    {provide: PromotionService, useClass: AfPromotionService},
    {provide: ProductService, useClass: AfProductService},
    {provide: AccountScoreService, useClass: AfAccountScoreService},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
