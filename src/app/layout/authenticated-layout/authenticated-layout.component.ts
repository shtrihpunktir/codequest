import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PageService } from '../../pages/page.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'cq-authenticated-layout',
  templateUrl: './authenticated-layout.component.html',
  styleUrls: ['./authenticated-layout.component.scss'],
  providers: [PageService]
})
export class AuthenticatedLayoutComponent implements OnInit {

  constructor(
    private location: Location,
    private pageService: PageService,
    private afAuth: AngularFireAuth,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
  }

  goBack() {
    this.location.back();
  }

  async logout() {
    await this.afAuth.auth.signOut();
    await this.router.navigate(['/login']);
  }
}
