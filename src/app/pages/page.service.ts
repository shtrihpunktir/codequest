import { ElementRef, Injectable, Renderer2 } from '@angular/core';

@Injectable()
export class PageService {

  constructor(
    private elRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {
  }

  initialize() {
    this.renderer.addClass(this.elRef.nativeElement, 'flex-grow-1');
    this.renderer.addClass(this.elRef.nativeElement, 'd-flex');
    this.renderer.addClass(this.elRef.nativeElement, 'flex-column');
  }
}
