import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';

@Component({
  selector: 'cq-promotion-list',
  templateUrl: './promotion-list-page.component.html',
  styleUrls: ['./promotion-list-page.component.scss'],
  providers: [PageService]
})
export class PromotionListPageComponent implements OnInit {

  constructor(
    private pageService: PageService
  ) { }

  ngOnInit() {
    this.pageService.initialize();
  }

}
