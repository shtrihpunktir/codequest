import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';

@Component({
  selector: 'cq-shop',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.scss'],
  providers: [PageService]
})
export class ShopPageComponent implements OnInit {

  constructor(
    private pageService: PageService
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
  }
}
