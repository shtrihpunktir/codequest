import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'cq-promotion-detail',
  templateUrl: './promotion-detail-page.component.html',
  styleUrls: ['./promotion-detail-page.component.scss'],
  providers: [PageService]
})
export class PromotionDetailPageComponent implements OnInit {

  id$ = this.route.paramMap.pipe(map(params => params.get('promotion')));

  constructor(
    private pageService: PageService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
  }

}
