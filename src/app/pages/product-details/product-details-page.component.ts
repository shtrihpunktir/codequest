import { Component, OnInit, ViewChild } from '@angular/core';
import { PageService } from '../page.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cq-product-details',
  templateUrl: './product-details-page.component.html',
  styleUrls: ['./product-details-page.component.scss'],
  providers: [PageService]
})
export class ProductDetailsPageComponent implements OnInit {
  @ViewChild('modal', {static: true}) modalTpl;
  id$ = this.route.paramMap.pipe(map(params => params.get('product')));
  afterBought = () => {
    this.modal.open(this.modalTpl, {
      centered: true,
    });
  }

  constructor(
    private pageService: PageService,
    private route: ActivatedRoute,
    private modal: NgbModal
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
  }

}
