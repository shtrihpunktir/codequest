import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Subject } from 'rxjs';
import { exhaustMap, tap } from 'rxjs/operators';

@Component({
  selector: 'cq-code-registration',
  templateUrl: './code-registration-page.component.html',
  styleUrls: ['./code-registration-page.component.scss'],
  providers: [PageService]
})
export class CodeRegistrationPageComponent implements OnInit {
  added: boolean = false;
  add = this.fns.httpsCallable('registerCode');
  submit = new Subject();
  public promotionId: string;
  public price: number;
  public submitting = new Subject();

  constructor(
    private pageService: PageService,
    private fns: AngularFireFunctions
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
    this.submit.pipe(
      tap(() => this.submitting.next(true)),
      exhaustMap((code) => this.add({code}))
    ).subscribe(({promotion, price}) => {
      this.added = true;
      this.promotionId = promotion;
      this.price = price;
      this.submitting.next(false);
    });
  }

  register(value: string) {
    if (!value || value.length < 6) {
      return;
    }
    this.submit.next(value);
  }
}
