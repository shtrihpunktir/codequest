import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';

@Component({
  selector: 'cq-home',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  providers: [PageService]
})
export class HomePageComponent implements OnInit {

  constructor(
    private page: PageService
  ) {
  }

  ngOnInit() {
    this.page.initialize();
  }

}
