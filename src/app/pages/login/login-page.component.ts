import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageService } from '../page.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { auth } from 'firebase';

@Component({
  selector: 'cq-login',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  providers: [PageService]
})
export class LoginPageComponent implements OnInit {
  @ViewChild('recaptcha', {static: true}) recaptchaContainer: ElementRef;
  formPhoneRequest = this.fb.group({
    phone: ['', Validators.required],
  });
  formCodeRequest = this.fb.group({
    code: ['', Validators.required],
  });
  private recaptcha: firebase.auth.RecaptchaVerifier;
  private confirmationResult: firebase.auth.ConfirmationResult;
  public requestCode = false;

  constructor(
    private pageService: PageService,
    private afAuth: AngularFireAuth,
    private fb: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.pageService.initialize();
    this.recaptcha = new auth.RecaptchaVerifier(this.recaptchaContainer.nativeElement, {
      size: 'invisible'
    });
  }

  async login() {
    if (this.formPhoneRequest.invalid) {
      return;
    }
    const {phone} = this.formPhoneRequest.value;
    this.confirmationResult = await this.afAuth.auth.signInWithPhoneNumber(phone, this.recaptcha);
    this.requestCode = true;
  }

  async confirm() {
    const {code} = this.formCodeRequest.value;
    await this.confirmationResult.confirm(code);
    this.router.navigate(['/']);
  }
}
