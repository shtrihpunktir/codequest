import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();
const db = admin.firestore();

export const createAccount = functions.auth.user().onCreate(async (user) => {
  await createTransaction('ACCOUNT_CREATED', user.uid);
});
export const registerCode = functions.https.onCall(async (data, context) => {
  const promotion = await db.collection('promotions').limit(1).get();
  const price = 250;
  await createTransaction('REGISTER_CODE', context.auth!.uid, {
    code: data.code,
    price,
    promotion: promotion.docs[0].id
  });
  return {promotion: promotion.docs[0].id, price};
});

export const buyProduct = functions.https.onCall(async (data, context) => {
  const {product} = data;
  const productRef = await db.doc(`products/${product}`).get();
  const price = productRef.get('price');
  await createTransaction(
    'BUY', context.auth!.uid, {
      product,
      name: productRef.get('title'),
      price,
    }
  );
});
export const transactionsProcessing = functions.firestore.document('transactions/{id}').onCreate(async (snapshot, context) => {
  const type = snapshot.get('type');
  const user = snapshot.get('user');
  const payload = snapshot.get('payload');
  let price;
  switch (type) {
    case 'ACCOUNT_CREATED':
      price = 1000;
      break;
    case 'REGISTER_CODE':
      price = payload.price;
      break;
    case 'BUY':
      price = -payload.price;
      break;
    default:
      price = 0;
  }
  const doc = await db.doc(`account/${user}`).get();
  if (!doc.exists) {
    await db.doc(`account/${user}`).create({
      value: price
    });
  } else {
    await db.doc(`account/${user}`).update('value', admin.firestore.FieldValue.increment(price));
  }
});

async function createTransaction(type: string, user: string, payload?: any) {
  let data: Record<any, any> = {
    type,
    user,
    createdAt: new Date()
  };
  if (payload) {
    data = {...data, payload};
  }
  await db.collection('transactions').add(data);
}
